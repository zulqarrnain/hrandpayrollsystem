﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HRandPayrollSystemModel.DBModel
{
    public partial class ProjectActivityLog
    {

        public bool IsView { get; set; }
        public bool Isedit { get; set; }
        public bool Isdelete { get; set; }
        public bool IsPrint { get; set; }
        public bool IsNew { get; set; }

        public IEnumerable<SelectListItem> LoadSericeTypes()
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {

                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "-- Please Select --", Value = "0" });

                    listobj.AddRange(context.tblProjectServices.Select(x => new SelectListItem { Text = x.ServiceDetail, Value = x.Service_ID.ToString() }).ToList());

                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public long addata(ProjectActivityLog obj)
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {


                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            
                            context.ProjectActivityLogs.Add(obj);
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback(); //Required according to MSDN article 
                            throw; //Not in MSDN article, but recommended so the exception still bubbles up
                        }
                    }




                    return 1;
                }




            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public IEnumerable<SelectListItem> loadEmployee()
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {

                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "-- Please Select --", Value = "0" });
                    listobj.AddRange(context.tblEmployees.Where(x => x.DepartmentID == 124 && x.inactive==false).Select(x => new SelectListItem { Text = x.EmployeeName.ToUpper(), Value = x.EmployeeID.ToString() }).ToList());

                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public long update(ProjectActivityLog obj)
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {


                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                           var result = context.ProjectActivityLogs.FirstOrDefault(x => x.Idlog == obj.Idlog);
                           if (result!=null)
                            {

                                result.ServiceActive = obj.ServiceActive;
                                result.Employeeid = obj.Employeeid;
                                result.Modifedyid = obj.Modifedyid;
                                result.Modifiedip = obj.Modifiedip;
                                result.Modifeddate = obj.Modifeddate;
                                context.SaveChanges();
                            }

                           
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback(); //Required according to MSDN article 
                            throw; //Not in MSDN article, but recommended so the exception still bubbles up
                        }
                    }




                    return 1;
                }




            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public List<sp_getSerivelogdetail_Result> getalldata()
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {
                   return context.sp_getSerivelogdetail().ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public List<sp_getSerivelogdetailviewlist_Result> getalldataviewlist()
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {
                    return context.sp_getSerivelogdetailviewlist().ToList();

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public IEnumerable<SelectListItem> loadAllActive()
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {

                    List<SelectListItem> listobj = new List<SelectListItem>();
                    listobj.Add(new SelectListItem { Text = "Active", Value = "Active" });
                    listobj.Add(new SelectListItem { Text = "Inactive", Value = "Inactive" });


                    return listobj;

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }



        public ProjectActivityLog getalldataedit(int id )
        {
            try
            {
                using (var context = new HRandPayrollDBEntities())
                {
                    return context.ProjectActivityLogs.FirstOrDefault(x=>x.Idlog==id);

                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }





    }
}