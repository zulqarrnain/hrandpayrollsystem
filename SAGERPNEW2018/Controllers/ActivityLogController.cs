﻿using HRandPayrollSystemModel.DBModel;
using Newtonsoft.Json;
using SAGERPNEW2018.Filters;
using SAGERPNEW2018.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SAGERPNEW2018.Controllers
{

    [NoCache]
    [OutputCache(Location = System.Web.UI.OutputCacheLocation.None, NoStore = true)]
    public class ActivityLogController : Controller
    {
        // GET: ActivityLog
        [UserRightFilters]
        public ActionResult Index()
        {
            ProjectActivityLog a = new ProjectActivityLog();
            a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
            a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
            a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
            a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);

            
            return View(a);
        }
        [UserRightFilters]
        public ActionResult create(ProjectActivityLog a)
        {
            try
            {
                a.IsNew = Convert.ToBoolean(TempData["IsNew"]);
                a.Isedit = Convert.ToBoolean(TempData["IsEdit"]);
                a.Isdelete = Convert.ToBoolean(TempData["IsDelete"]);
                a.IsPrint = Convert.ToBoolean(TempData["IsPrint"]);
              
               

                return View(a);
            }
            catch (Exception ex)
            {

                return View(a);
            }

        }

        public ActionResult Save(ProjectActivityLog model, FormCollection collection)
        {
            long check=0;

            if (model.Idlog > 0)
            {
                model.Modifeddate = DateTime.Now;
                model.Modifedyid = new SAGERPNEW2018.Models.SystemLogin().GetUser().Userid; 
                model.Modifiedip = Request.UserHostAddress;
                check = model.update(model);

            }
            else
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedIP = Request.UserHostAddress;
                model.CreatedBy = new SAGERPNEW2018.Models.SystemLogin().GetUser().Userid;
                check = model.addata(model);

            }

          

            if (check > 0)
            {
                ViewData["Message"] = "Success";
                return RedirectToAction("Index");

            }
            return RedirectToAction("create", model);
        }

        [UserRightFilters]

        public ActionResult Edit(string id)
        {
            try
            {
                string[] IDo = id.Split('|');

                ProjectActivityLog a = new ProjectActivityLog();
                var obj = a.getalldataedit(Convert.ToInt32(IDo[0]));
                if (IDo[1] == "0")
                {
                    a.IsView = true;
                }
                obj.IsView = a.IsView;
                obj.Idlog = Convert.ToInt32(IDo[0]);
                return View("create", obj);

            }
            catch (Exception ex)
            {
                return View("create");
            }

        }


        public ActionResult viewlist()
        {
            ProjectActivityLog a = new ProjectActivityLog();
            

            return View(a);
        }


    }
}